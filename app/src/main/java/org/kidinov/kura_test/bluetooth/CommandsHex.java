package org.kidinov.kura_test.bluetooth;

public class CommandsHex {
    public static final byte[] TOC = new byte[]{0x41, 0x54, 0x2B, 0x4B, 0x45, 0x59, 0x3D, 0x31, 0x2C, 0x30, 0x0D, 0x0A};
    public static final byte[] PAGE_DOWN = new byte[]{0x41, 0x54, 0x2B, 0x4B, 0x45, 0x59, 0x3D, 0x32, 0x2C, 0x30, 0x0D, 0x0A};
    public static final byte[] PAGE_UP = new byte[]{0x41, 0x54, 0x2B, 0x4B, 0x45, 0x59, 0x3D, 0x33, 0x2C, 0x30, 0x0D, 0x0A};

}

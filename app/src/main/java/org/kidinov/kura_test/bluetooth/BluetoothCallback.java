package org.kidinov.kura_test.bluetooth;

public interface BluetoothCallback {
    void showTableOfContents();

    void pageUp();

    void pageDown();
}

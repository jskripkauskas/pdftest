package org.kidinov.kura_test.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.AsyncTask;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.UUID;

public class ConnectorTask extends AsyncTask<BluetoothDevice, Boolean, Boolean> {
    private BluetoothDevice device;
    private BluetoothAdapter bluetoothAdapter;
    private BluetoothSocket socket;
    private InputStream inputStream;
    private static ConnectorReaderCallback callback;

    public ConnectorTask(BluetoothAdapter bluetoothAdapter, ConnectorReaderCallback callback) {
        this.bluetoothAdapter = bluetoothAdapter;
        ConnectorTask.callback = callback;
    }

    @Override
    protected Boolean doInBackground(BluetoothDevice... disp) {
        device = disp[0];
        bluetoothAdapter.cancelDiscovery();
        return connect();
    }

    protected Boolean connect() {
        try {
            socket = device.createInsecureRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
            socket.connect();
            inputStream = socket.getInputStream();
            return true;
        } catch (Exception e) {
            try {
                if (socket == null || socket.getRemoteDevice() == null) {
                    callback.retry();
                    return false;
                }
                Class<?> clazz = socket.getRemoteDevice().getClass();
                Class<?>[] paramTypes = new Class<?>[]{Integer.TYPE};
                Method m = clazz.getMethod("createInsecureRfcommSocket", paramTypes);
                Object[] params = new Object[]{1};
                socket = (BluetoothSocket) m.invoke(socket.getRemoteDevice(), params);
                Thread.sleep(500);
                socket.connect();
            } catch (Exception e1) {
                callback.retry();
            }
        }
        return false;
    }

    @Override
    protected void onPostExecute(Boolean res) {
        if (res) {
            new ReceiverTask(inputStream, callback).execute(device);
        }
    }

    public void close() {
        try {
            if (socket != null) {
                socket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void setCallback(ConnectorReaderCallback callback) {
        ConnectorTask.callback = callback;
    }
}

package org.kidinov.kura_test.bluetooth;

import android.bluetooth.BluetoothDevice;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class ReceiverTask extends AsyncTask<BluetoothDevice, String, String> {
    private static final int MAX_BUFFER_SIZE = 1024;
    private static final String TAG = ReceiverTask.class.getName();
    private InputStream inputStream;
    private BluetoothDevice device;
    private boolean enable = true;
    private static ConnectorReaderCallback callback;

    public ReceiverTask(InputStream inputStream, ConnectorReaderCallback callback) {
        this.inputStream = inputStream;
        ReceiverTask.callback = callback;
    }

    protected String doInBackground(BluetoothDevice... params) {
        device = params[0];
        String message = "";
        ByteBuffer buffer = fillBuffer(inputStream);

        while (enable) {
            try {
                if (buffer.remaining() > 0) {
                    checkCommand(buffer);
                }
                refillBuffer(buffer, inputStream);
            } catch (IOException e) {
                enable = false;
                callback.retry();
            }
        }
        return message;
    }

    protected void checkCommand(ByteBuffer buffer) throws IOException {
        byte[] bytes = new byte[buffer.remaining()];
        buffer.get(bytes, 0, bytes.length);

        if (Arrays.equals(CommandsHex.TOC, bytes)) {
            callback.tableOfContents();
        } else if (Arrays.equals(CommandsHex.PAGE_DOWN, bytes)) {
            callback.pageDown();
        } else if (Arrays.equals(CommandsHex.PAGE_UP, bytes)) {
            callback.pageUp();
        }
    }

    protected void refillBuffer(ByteBuffer buffer, InputStream is) throws IOException {
        int newCapacity = buffer.capacity() - buffer.remaining();
        if (newCapacity == 0) {
            return;
        }

        byte[] newData = new byte[newCapacity];
        int bytesRead = is.read(newData);
        int newLimit = buffer.remaining() + bytesRead;
        buffer.compact();
        buffer.put(newData, 0, bytesRead);
        buffer.rewind();
        buffer.limit(newLimit);
    }

    protected ByteBuffer fillBuffer(InputStream inputStream) {
        byte[] array = new byte[MAX_BUFFER_SIZE];
        int bytesRead = 0;
        try {
            bytesRead = inputStream.read(array);
        } catch (Exception e) {
            Log.e(TAG, "Error reading from device: " + device.getName() + ":" + e.getMessage());
        }

        return ByteBuffer.wrap(array, 0, bytesRead);
    }

    public static void setCallback(ConnectorReaderCallback callback) {
        ReceiverTask.callback = callback;
    }
}

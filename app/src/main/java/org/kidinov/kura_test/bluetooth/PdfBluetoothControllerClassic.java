package org.kidinov.kura_test.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;

import java.util.Set;

public class PdfBluetoothControllerClassic implements ConnectorReaderCallback {
    private static final String TAG = "BluetoothClassic";
    private BluetoothAdapter bluetoothAdapter;
    private static BluetoothCallback controllerCallback;
    private ConnectorTask connector;

    public PdfBluetoothControllerClassic() {
    }

    public void connect() {
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (bluetoothAdapter != null && bluetoothAdapter.isEnabled()) {
            Set<BluetoothDevice> bonded = bluetoothAdapter.getBondedDevices();
            if (bonded.size() > 0) {
                connectToPaired(bonded);
            }
        }
    }

    public void close() {
        if (connector != null) {
            connector.connect();
        }
    }

    @Override
    public void retry() {
        connector.close();
        connect();
    }

    @Override
    public void tableOfContents() {
        if (controllerCallback != null) {
            controllerCallback.showTableOfContents();
        }
    }

    @Override
    public void pageDown() {
        if (controllerCallback != null) {
            controllerCallback.pageDown();
        }
    }

    @Override
    public void pageUp() {
        if (controllerCallback != null) {
            controllerCallback.pageUp();
        }
    }

    public static void setBluetoothController(BluetoothCallback bluetoothController) {
        controllerCallback = bluetoothController;
    }

    private void connectToPaired(Set<BluetoothDevice> bonded) {
        for (BluetoothDevice device : bonded) {
            //BT_APK_2
            if (device.getName().toUpperCase().contains("BT_APK_2")) {
                connector = new ConnectorTask(bluetoothAdapter, this);
                connector.execute(device);
            }
        }
    }
}
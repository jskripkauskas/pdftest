package org.kidinov.kura_test.bluetooth;

public interface ConnectorReaderCallback {

    void tableOfContents();

    void pageDown();

    void pageUp();

    void retry();
}

package org.kidinov.kura_test.pdf_reader_opened_file.domain.interactor;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.SimpleBookmark;
import com.itextpdf.text.pdf.SimpleNamedDestination;

import org.kidinov.kura_test.common.NumberUtils;
import org.kidinov.kura_test.pdf_reader_opened_file.data.TocElement;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class PdfReaderOpenedFileToc {
    private ArrayList<TocElement> tableOfContents;

    public PdfReaderOpenedFileToc() {
        tableOfContents = new ArrayList<>();
    }

    public void generateToc(String src) {
        try {
            PdfReader reader = new PdfReader(src);
            HashMap<String, String> namedDestinations = SimpleNamedDestination.getNamedDestination(reader, false);
            List<HashMap<String, Object>> bookmarks = SimpleBookmark.getBookmark(reader);
            mapToc(namedDestinations, bookmarks, 0);
            Collections.sort(tableOfContents);
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void mapToc(HashMap<String, String> namedDestinations, List<HashMap<String, Object>> bookmarks, int level) {
        for (HashMap<String, Object> bookmark : bookmarks) {
            TocElement tocElement = new TocElement();
            String named = (String) bookmark.get("Named");
            String page = namedDestinations.get(named).replace(" Fit", "");

            if (NumberUtils.isNumeric(page)) {
                tocElement.title = (String) bookmark.get("Title");
                tocElement.page = Integer.parseInt(page);
                tocElement.level = level;
            }

            List<HashMap<String, Object>> kids = (List<HashMap<String, Object>>) bookmark.get("Kids");
            if (kids != null) {
                mapToc(namedDestinations, kids, level + 1);
            }
            tableOfContents.add(tocElement);
        }
    }

    public ArrayList<TocElement> getTableOfContents() {
        return tableOfContents;
    }
}

package org.kidinov.kura_test.pdf_reader_opened_file.presentation;

import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SeekBar;

import com.h6ah4i.android.widget.verticalseekbar.VerticalSeekBar;
import com.joanzapata.pdfview.PDFView;
import com.joanzapata.pdfview.util.Constants;

import org.kidinov.kura_test.R;
import org.kidinov.kura_test.common.BaseActivity;
import org.kidinov.kura_test.common.Const;
import org.kidinov.kura_test.common.DisplayUtils;
import org.kidinov.kura_test.common.Presenter;
import org.kidinov.kura_test.databinding.PdfReaderOpenedFileActivityBinding;
import org.kidinov.kura_test.pdf_reader_opened_file.di.DaggerPdfReaderOpenedFileComponent;
import org.kidinov.kura_test.pdf_reader_opened_file.di.PdfReaderOpenedFileComponent;
import org.kidinov.kura_test.pdf_reader_opened_file.di.PdfReaderOpenedFileModule;
import org.kidinov.kura_test.pdf_reader_opened_file.domain.interactor.PdfReaderOpenedFileToc;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscription;

public class PdfReaderOpenedFileActivity extends BaseActivity implements PdfReaderOpenedFileView, OnPdfContainerListener {
    @Inject
    PdfReaderOpenedFileViewModel viewModel;
    @Inject
    PdfReaderOpenedFilePresenter presenter;
    @Inject
    PdfReaderOpenedFileToc tocGenerator;
    //We should avoid direct usage of view, instead of it we should manipulate with ViewModel
    //Here it was done only in order to avoid extra complexity
    private Toolbar toolbar, titleBar;
    private PDFView pdfView;
    private VerticalSeekBar seekBar;
    private Subscription hidePageDelayedSubscription;
    private boolean systemUiShown = false;
    private PdfReaderOpenedFileTocDialog tocDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PdfReaderOpenedFileActivityBinding binding = DataBindingUtil.setContentView(this, R.layout.pdf_reader_opened_file_activity);
        binding.setViewModel(viewModel);

        initViews();
        setupSeekBar();

        setSupportActionBar(titleBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            View decorView = getWindow().getDecorView();
            decorView.setOnSystemUiVisibilityChangeListener(visibility -> {
                if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == View.VISIBLE) {
                    toolbar.setVisibility(visibility);
                    titleBar.setVisibility(visibility);
                } else {
                    toolbar.setVisibility(View.GONE);
                    titleBar.setVisibility(View.GONE);
                }
            });
            hideSystemUI(decorView);
        }
    }

    private void initViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        createMenu();
        titleBar = (Toolbar) findViewById(R.id.titleBar);
        seekBar = (VerticalSeekBar) findViewById(R.id.seek_bar);

        pdfView = (PDFView) findViewById(R.id.pdf_view);
        pdfView.setDragPinchManager(new DragPinchManagerPdf(pdfView));
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        Constants.Pinch.MINIMUM_ZOOM = (metrics.widthPixels > metrics.heightPixels ? 1 : 0) +
                metrics.widthPixels / (float) metrics.heightPixels;

        PdfContainerLayout pdfContainer = (PdfContainerLayout) findViewById(R.id.pdf_container);
        pdfContainer.setOnPdfContainerListener(this);
    }

    private void drawProgress(SeekBar seekBar, int progress) {
        String dynamicText = String.valueOf(progress + 1);
        seekBar.setThumb(writeOnDrawable(dynamicText));
    }

    private void setupSeekBar() {
        drawProgress(seekBar, 0);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                drawProgress(seekBar, progress);
                if (fromUser) {
                    goToPage(progress + 1);
                    viewModel.getShowPageNumber().set(true);
                    if (hidePageDelayedSubscription != null) {
                        hidePageDelayedSubscription.unsubscribe();
                        hidePageDelayedSubscription = null;
                    }
                    hidePageDelayedSubscription = Observable.just(null)
                            .delay(1000, TimeUnit.MILLISECONDS)
                            .subscribe(x -> viewModel.getShowPageNumber().set(false));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }

    private void hideSystemUI(View decorView) {
        systemUiShown = false;
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
    }

    private void showSystemUI(View decorView) {
        systemUiShown = true;
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    private BitmapDrawable writeOnDrawable(String text) {
        int expansionSize = DisplayUtils.dpToPx(this, 5) * (text.length() - 1);
        int size = DisplayUtils.dpToPx(this, 30) + expansionSize;

        Bitmap bm = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bm);
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.parseColor("#99000000"));
        canvas.drawCircle(size / 2, size / 2, size / 2, paint);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.WHITE);
        paint.setTextSize((size - expansionSize) / 2);
        int xPos = (size / 2) + 15;
        int yPos = (int) ((size / 2) - ((paint.descent() + paint.ascent()) / 2));
        canvas.rotate(-90, xPos, yPos);
        canvas.drawText(text, xPos - expansionSize / (text.length() + 1) * text.length(), yPos, paint);

        return new BitmapDrawable(getResources(), bm);
    }

    public void createMenu() {
        ImageButton openFile = (ImageButton) toolbar.findViewById(R.id.pdf_menu_open_file);
        ImageButton toc = (ImageButton) toolbar.findViewById(R.id.pdf_menu_table_of_content);
        openFile.setOnClickListener(v -> startPdfFilePickerActivity(Const.OPEN_FILE_CODE));
        toc.setOnClickListener(v -> showTableOfContentDialog());

        DrawableCompat.setTint(openFile.getDrawable(), Color.WHITE);
        DrawableCompat.setTint(toc.getDrawable(), Color.WHITE);
    }

    @Override
    public void showTableOfContentDialog() {
        if (tocDialog != null && tocDialog.isVisible()) {
            tocDialog.dismiss();
            int page = tocDialog.getSelected(pdfView.getCurrentPage());
            tocDialog = null;
            goToPage(page);
        } else if (!isFinishing()) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            tocDialog = PdfReaderOpenedFileTocDialog.newInstance(tocGenerator.getTableOfContents());
//            tocDialog.show(ft, "PdfReaderOpenedFileTocDialog");
            tocDialog.setSelected(pdfView.getCurrentPage() + 1);

            ft.add(tocDialog, "PdfReaderOpenedFileTocDialog").addToBackStack(null).commitAllowingStateLoss();
        }
    }

    @Override
    protected void setupComponent() {
        PdfReaderOpenedFileComponent component = DaggerPdfReaderOpenedFileComponent.builder()
                .applicationComponent(getApplicationComponent())
                .pdfReaderOpenedFileModule(new PdfReaderOpenedFileModule(this, this, getIntent().getData()))
                .build();

        component.inject(this);
    }

    @Override
    protected Presenter getPresenter() {
        return presenter;
    }

    @Override
    public void setFileTitle(String title) {
        titleBar.setTitle(title);
    }

    @Override
    public boolean goToPage(int pageNum) {
        if (tocDialog != null && tocDialog.isVisible()) {
            if (pdfView.getCurrentPage() < pageNum - 1) {
                tocDialog.goDown();
            } else {
                tocDialog.goUp();
            }
            return false;
        } else {
            runOnUiThread(() -> {
                pdfView.jumpTo(pageNum);
            });
            return true;
        }
    }

    @Override
    public void pageChanged() {
        seekBar.setMax(pdfView.getPageCount());
        seekBar.setProgress(pdfView.getCurrentPage());

        if (pdfView.getZoom() < Constants.Pinch.MINIMUM_ZOOM) {
            pdfView.zoomTo(Constants.Pinch.MINIMUM_ZOOM);
        }
    }

    @Override
    public void onContainerClick() {
        View decorView = getWindow().getDecorView();

        if (systemUiShown) {
            hideSystemUI(decorView);
        } else {
            showSystemUI(decorView);
        }
    }

}

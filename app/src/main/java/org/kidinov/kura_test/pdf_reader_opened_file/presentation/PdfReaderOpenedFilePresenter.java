package org.kidinov.kura_test.pdf_reader_opened_file.presentation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import org.kidinov.kura_test.bluetooth.BluetoothCallback;
import org.kidinov.kura_test.bluetooth.PdfBluetoothControllerClassic;
import org.kidinov.kura_test.common.Const;
import org.kidinov.kura_test.common.Presenter;
import org.kidinov.kura_test.pdf_reader_opened_file.domain.interactor.DefaultSubscriber;
import org.kidinov.kura_test.pdf_reader_opened_file.domain.interactor.GetFilePathCase;
import org.kidinov.kura_test.pdf_reader_opened_file.domain.interactor.PdfReaderOpenedFileToc;

import java.io.File;
import java.util.concurrent.TimeUnit;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

public class PdfReaderOpenedFilePresenter implements Presenter, BluetoothCallback {
    private final PdfReaderOpenedFileView view;
    private final PdfReaderOpenedFileViewModel viewModel;
    private final GetFilePathCase getFilePathCase;
    private Subscription pageChangedListenerSubscription;
    private Subscription loadCompleteListenerSubscription;
    private PdfReaderOpenedFileToc tocGenerator;
    private PdfBluetoothControllerClassic bluetoothController;
    private int currentPage = -1;

    public PdfReaderOpenedFilePresenter(PdfReaderOpenedFileView view, PdfReaderOpenedFileViewModel viewModel,
                                        GetFilePathCase getFilePathCase, PdfReaderOpenedFileToc tocGenerator,
                                        PdfBluetoothControllerClassic bluetoothController) {
        this.view = view;
        this.viewModel = viewModel;
        this.getFilePathCase = getFilePathCase;
        this.tocGenerator = tocGenerator;
        this.bluetoothController = bluetoothController;
    }

    @Override
    public void create(Bundle savedInstanceState) {
        //Just to demonstrate how remote calls should be done
        showPreloaderView();
        getFilePathCase.execute(new GetPathSubscriber());
        bluetoothController.connect();

        if (savedInstanceState != null) {
            currentPage = savedInstanceState.getInt("page");
        }

        loadCompleteListenerSubscription = viewModel.getLoadCompleteListener()
                .delay(100, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(x -> {
                    if (currentPage != -1) {
                        view.goToPage(currentPage);
                    }
                    view.pageChanged();
                });


        pageChangedListenerSubscription = viewModel.getPageChangedListener()
                //In order to restore proper position
                .skipUntil(viewModel.getLoadCompleteListener())
                .subscribe(pair -> {
                    currentPage = pair.first;
                    viewModel.getPageNumber().set(String.format("%d/%d", pair.first, pair.second));
                    view.pageChanged();
                });
    }

    @Override
    public void resume() {
        PdfBluetoothControllerClassic.setBluetoothController(this);
    }

    @Override
    public void pause() {
    }

    @Override
    public void saveInstanceState(Bundle outState) {
        outState.putInt("page", currentPage);
    }

    @Override
    public void destroy() {
        getFilePathCase.unsubscribe();
        pageChangedListenerSubscription.unsubscribe();
        loadCompleteListenerSubscription.unsubscribe();
        bluetoothController.close();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Const.OPEN_FILE_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    view.startPdfOpenedFileActivity(data.getData());
                }
                break;
        }
    }

    /**
     * Show preloader view
     * Need to be implemented in real app
     */
    private void showPreloaderView() {
    }

    //


    /**
     * Show something to user in case of error
     * Need to be implemented in real app
     */
    private void showErrorMessage() {
    }

    private void showPdf(String path) {
        File file = new File(path);
        viewModel.getFileToShow().set(file);
        view.setFileTitle(file.getName());
        view.pageChanged();
        tocGenerator.generateToc(path);
    }

    /**
     * Hide preloader view
     * Need to be implemented in real app
     */
    private void hideViewLoading() {
    }

    @Override
    public void showTableOfContents() {
        view.showTableOfContentDialog();
    }

    @Override
    public void pageUp() {
        int savedCurrent = currentPage;

        if (currentPage <= 0) {
            currentPage = 1;
        } else {
            currentPage -= 1;
        }
        currentPage = view.goToPage(currentPage) ? currentPage : savedCurrent;
    }

    @Override
    public void pageDown() {
        int savedCurrent = currentPage;

        if (currentPage <= 1) {
            currentPage = 2;
        } else {
            currentPage += 1;
        }
        currentPage = view.goToPage(currentPage) ? currentPage : savedCurrent;
    }

    private final class GetPathSubscriber extends DefaultSubscriber<String> {

        @Override
        public void onCompleted() {
            hideViewLoading();
        }

        @Override
        public void onError(Throwable e) {
            super.onError(e);
            hideViewLoading();
            showErrorMessage();
        }

        @Override
        public void onNext(String path) {
            showPdf(path);
        }
    }

}

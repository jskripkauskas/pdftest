package org.kidinov.kura_test.pdf_reader_opened_file.presentation;

public interface OnPdfContainerListener {

    void onContainerClick();
}

package org.kidinov.kura_test.pdf_reader_opened_file.presentation;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import org.kidinov.kura_test.R;
import org.kidinov.kura_test.pdf_reader_opened_file.data.TocElement;

public class PdfReaderOpenedFileTocAdapter extends RecyclerView.Adapter<TocRecyclerHolder> implements View.OnClickListener {
    private Context context;
    private TocElement[] tocElements;
    private int entryMargin;
    private DialogFragment dialog;
    private int selectionColor, nonSelectedColor;
    private int selected = 0;

    public PdfReaderOpenedFileTocAdapter(Context context, TocElement[] tocElements, DialogFragment dialog) {
        this.context = context;
        this.tocElements = tocElements;
        this.dialog = dialog;
        entryMargin = context.getResources().getDimensionPixelSize(R.dimen.table_of_content_entry_margin);
        selectionColor = context.getResources().getColor(R.color.tocSelectedColor);
        nonSelectedColor = context.getResources().getColor(android.R.color.transparent);

    }

    @Override
    public TocRecyclerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.toc_recycler_item, parent, false);
        view.setOnClickListener(this);
        return new TocRecyclerHolder(view);
    }

    @Override
    public void onBindViewHolder(TocRecyclerHolder holder, int position) {
        TocElement element = tocElements[position];
        holder.binding.setTocModel(element);

        if (position == selected) {
            holder.binding.getRoot().setBackgroundColor(selectionColor);
        } else {
            holder.binding.getRoot().setBackgroundColor(nonSelectedColor);
        }
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.binding.title.getLayoutParams();
        params.setMarginStart(element.level * entryMargin);
    }

    @Override
    public int getItemCount() {
        return tocElements.length;
    }

    @Override
    public void onClick(View v) {
        TocRecyclerHolder holder = (TocRecyclerHolder) v.getTag();
        TocElement tocElement = holder.binding.getTocModel();
        dialog.dismiss();
        ((PdfReaderOpenedFileView) context).goToPage(tocElement.page);
    }

    public int getSelected() {
        return tocElements[selected].page;
    }

    public void goDown(Activity activity) {
        selected++;
        if (selected > tocElements.length) {
            selected = tocElements.length - 1;
        }
        activity.runOnUiThread(this::notifyDataSetChanged);
    }

    public void goUp(Activity activity) {
        selected--;
        if (selected < 0) {
            selected = 0;
        }
        activity.runOnUiThread(this::notifyDataSetChanged);
    }

    public void setSelected(int selected, Activity activity) {
        for (int index = 0; index < tocElements.length; index++) {
            TocElement tocElement = tocElements[index];

            //TODO select closest value instead
            if (tocElement.page == selected) {
                this.selected = index;
            }
        }
        activity.runOnUiThread(this::notifyDataSetChanged);
    }
}

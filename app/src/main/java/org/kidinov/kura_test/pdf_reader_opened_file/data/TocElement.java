package org.kidinov.kura_test.pdf_reader_opened_file.data;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

public class TocElement implements Comparable<TocElement>, Parcelable {
    public String title;
    public int page;
    public int level;

    @Override
    public int compareTo(@NonNull TocElement another) {
        if (page > another.page) {
            return 1;
        } else if (page < another.page) {
            return -1;
        } else {
            return 0;
        }
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeInt(this.page);
        dest.writeInt(this.level);
    }

    public TocElement() {
    }

    protected TocElement(Parcel in) {
        this.title = in.readString();
        this.page = in.readInt();
        this.level = in.readInt();
    }

    public static final Parcelable.Creator<TocElement> CREATOR = new Parcelable.Creator<TocElement>() {
        public TocElement createFromParcel(Parcel source) {
            return new TocElement(source);
        }

        public TocElement[] newArray(int size) {
            return new TocElement[size];
        }
    };
}

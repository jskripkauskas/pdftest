package org.kidinov.kura_test.pdf_reader_opened_file.presentation;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import org.kidinov.kura_test.databinding.TocRecyclerItemBinding;

public class TocRecyclerHolder extends RecyclerView.ViewHolder {
    public TocRecyclerItemBinding binding;

    public TocRecyclerHolder(View itemView) {
        super(itemView);

        binding = DataBindingUtil.bind(itemView);
        itemView.setTag(this);
    }

}
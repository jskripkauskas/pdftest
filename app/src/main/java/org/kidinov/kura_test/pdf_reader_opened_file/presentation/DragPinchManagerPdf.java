package org.kidinov.kura_test.pdf_reader_opened_file.presentation;

import android.graphics.PointF;

import com.joanzapata.pdfview.DragPinchManager;
import com.joanzapata.pdfview.PDFView;
import com.joanzapata.pdfview.util.DragPinchListener;

import static com.joanzapata.pdfview.util.Constants.Pinch.MAXIMUM_ZOOM;
import static com.joanzapata.pdfview.util.Constants.Pinch.MINIMUM_ZOOM;

public class DragPinchManagerPdf extends DragPinchManager {
    private PDFView pdfView;
    private DragPinchListener dragPinchListener;
    private boolean isSwipeEnabled;

    public DragPinchManagerPdf(PDFView pdfView) {
        super(pdfView);
        this.pdfView = pdfView;
        this.isSwipeEnabled = true;
        dragPinchListener = new DragPinchListener();
        dragPinchListener.setOnDragListener(this);
        dragPinchListener.setOnPinchListener(this);
        dragPinchListener.setOnDoubleTapListener(this);
        pdfView.setOnTouchListener(dragPinchListener);
    }

    public void enableDoubletap(boolean enableDoubletap) {
        if (enableDoubletap) {
            dragPinchListener.setOnDoubleTapListener(this);
        } else {
            dragPinchListener.setOnDoubleTapListener(null);
        }
    }

    @Override
    public void onPinch(float dr, PointF pivot) {
        float wantedZoom = pdfView.getZoom() * dr;

        if (wantedZoom < MINIMUM_ZOOM || wantedZoom < 1) {
            return;
        } else if (wantedZoom > MAXIMUM_ZOOM) {
            dr = MAXIMUM_ZOOM / pdfView.getZoom();
        }
        pdfView.zoomCenteredRelativeTo(dr, pivot);
    }

    @Override
    public void onDrag(float dx, float dy) {
        float max = -pdfView.getOptimalPageHeight() * pdfView.getPageCount() + pdfView.getOptimalPageHeight();

        if (((pdfView.getCurrentYOffset() < 0 || dy < 0) &&
                (pdfView.getCurrentYOffset() > max || dy > 0))) {
            pdfView.moveRelativeTo(dx, dy);
        }
    }

    @Override
    public void endDrag(float x, float y) {
        float zoom = pdfView.getZoom();
        int currentPage = Math.round(Math.abs(pdfView.getCurrentYOffset()) / pdfView.getOptimalPageHeight() / zoom);

        if (isSwipeEnabled && currentPage < pdfView.getPageCount()) {
            pdfView.showPage(currentPage, true);
        }
        pdfView.setZoom(zoom);
    }

    public boolean isZooming() {
        return pdfView.isZooming();
    }

    public void setSwipeEnabled(boolean isSwipeEnabled) {
        this.isSwipeEnabled = isSwipeEnabled;
    }

    @Override
    public void onDoubleTap(float x, float y) {
        if (isZooming()) {
            pdfView.resetZoomWithAnimation();
        }
    }

}

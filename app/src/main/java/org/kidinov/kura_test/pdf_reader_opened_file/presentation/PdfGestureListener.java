package org.kidinov.kura_test.pdf_reader_opened_file.presentation;

import android.view.GestureDetector;
import android.view.MotionEvent;

public class PdfGestureListener extends GestureDetector.SimpleOnGestureListener {
    private OnPdfContainerListener onPdfContainerListener;

    public PdfGestureListener(OnPdfContainerListener onPdfContainerListener) {
        this.onPdfContainerListener = onPdfContainerListener;
    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        onPdfContainerListener.onContainerClick();
        return false;
    }

}

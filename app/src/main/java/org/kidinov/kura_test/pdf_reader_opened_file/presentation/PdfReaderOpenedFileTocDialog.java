package org.kidinov.kura_test.pdf_reader_opened_file.presentation;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.kidinov.kura_test.R;
import org.kidinov.kura_test.pdf_reader_opened_file.data.TocElement;

import java.util.ArrayList;

public class PdfReaderOpenedFileTocDialog extends DialogFragment {
    private TocElement[] tableOfContent;
    private RecyclerView tocList;
    private int selected = -1;

    static PdfReaderOpenedFileTocDialog newInstance(ArrayList<TocElement> tableOfContent) {
        PdfReaderOpenedFileTocDialog f = new PdfReaderOpenedFileTocDialog();

        Bundle args = new Bundle();
        TocElement[] tempTocArray = new TocElement[tableOfContent.size()];
        args.putParcelableArray("tableOfContent", tableOfContent.toArray(tempTocArray));
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tableOfContent = (TocElement[]) getArguments().getParcelableArray("tableOfContent");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.table_of_content_dialog, container, false);
        if (tableOfContent.length > 0) {
            tocList = (RecyclerView) view.findViewById(R.id.table_of_content_list);
            tocList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
            tocList.setAdapter(new PdfReaderOpenedFileTocAdapter(getActivity(), tableOfContent, this));
            setSelected(selected);
        } else {
            view.findViewById(R.id.table_of_content_none).setVisibility(View.VISIBLE);
        }
        return view;
    }

    public int getSelected(int defaultValue) {
        if (tableOfContent.length > 0) {
            return ((PdfReaderOpenedFileTocAdapter) tocList.getAdapter()).getSelected();
        }
        return defaultValue;
    }

    public void goDown() {
        if (tableOfContent.length > 0) {
            ((PdfReaderOpenedFileTocAdapter) tocList.getAdapter()).goDown(getActivity());
        }
    }

    public void goUp() {
        if (tableOfContent.length > 0) {
            ((PdfReaderOpenedFileTocAdapter) tocList.getAdapter()).goUp(getActivity());
        }
    }

    public void setSelected(int selected) {
        if (tableOfContent == null) {
            this.selected = selected;
            return;
        }
        if (tableOfContent.length > 0 && selected > -1) {
            ((PdfReaderOpenedFileTocAdapter) tocList.getAdapter()).setSelected(selected, getActivity());
        }
    }
}

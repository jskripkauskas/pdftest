package org.kidinov.kura_test.pdf_reader_opened_file.di;

import android.app.Activity;
import android.net.Uri;

import org.kidinov.kura_test.bluetooth.PdfBluetoothControllerClassic;
import org.kidinov.kura_test.pdf_reader_opened_file.domain.interactor.GetFilePathCase;
import org.kidinov.kura_test.pdf_reader_opened_file.domain.interactor.PdfReaderOpenedFileToc;
import org.kidinov.kura_test.pdf_reader_opened_file.presentation.PdfReaderOpenedFilePresenter;
import org.kidinov.kura_test.pdf_reader_opened_file.presentation.PdfReaderOpenedFileView;
import org.kidinov.kura_test.pdf_reader_opened_file.presentation.PdfReaderOpenedFileViewModel;

import dagger.Module;
import dagger.Provides;

@Module
public class PdfReaderOpenedFileModule {
    private final Activity activity;
    private PdfReaderOpenedFileView view;
    private final Uri fileUri;

    public PdfReaderOpenedFileModule(Activity activity, PdfReaderOpenedFileView view, Uri fileUri) {
        this.activity = activity;
        this.view = view;
        this.fileUri = fileUri;
    }

    @Provides
    @PerPdfReaderOpenedFileActivity
    public PdfReaderOpenedFilePresenter providePresenter(PdfReaderOpenedFileViewModel viewModel,
                                                         GetFilePathCase getFilePathCase, PdfReaderOpenedFileToc tocGenerator,
                                                         PdfBluetoothControllerClassic bluetoothController) {
        return new PdfReaderOpenedFilePresenter(view, viewModel, getFilePathCase, tocGenerator, bluetoothController);
    }

    @Provides
    @PerPdfReaderOpenedFileActivity
    public PdfReaderOpenedFileViewModel provideViewModel() {
        return new PdfReaderOpenedFileViewModel();
    }

    @Provides
    @PerPdfReaderOpenedFileActivity
    public GetFilePathCase provideUseCase() {
        return new GetFilePathCase(activity, fileUri);
    }

    @Provides
    @PerPdfReaderOpenedFileActivity
    public PdfReaderOpenedFileToc provideTocGenerator() {
        return new PdfReaderOpenedFileToc();
    }

    @Provides
    @PerPdfReaderOpenedFileActivity
    public PdfBluetoothControllerClassic providesBluetoothController() {
        return new PdfBluetoothControllerClassic();
    }
}

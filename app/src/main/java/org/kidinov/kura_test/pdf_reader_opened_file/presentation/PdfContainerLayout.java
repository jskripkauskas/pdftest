package org.kidinov.kura_test.pdf_reader_opened_file.presentation;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v4.view.GestureDetectorCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.RelativeLayout;

public class PdfContainerLayout extends RelativeLayout {
    private GestureDetectorCompat detector;
    private OnPdfContainerListener onPdfContainerListener;

    public PdfContainerLayout(Context context) {
        super(context);
    }

    public PdfContainerLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PdfContainerLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public PdfContainerLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (detector != null) {
            detector.onTouchEvent(event);
        }
        return false;
    }

    public void setOnPdfContainerListener(OnPdfContainerListener onPdfContainerListener) {
        this.onPdfContainerListener = onPdfContainerListener;
        detector = new GestureDetectorCompat(getContext(), new PdfGestureListener(onPdfContainerListener));
    }
}

package org.kidinov.kura_test.common;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;

import com.nononsenseapps.filepicker.FilePickerActivity;

import org.kidinov.kura_test.common.di.component.ActivityComponent;
import org.kidinov.kura_test.common.di.component.ApplicationComponent;
import org.kidinov.kura_test.common.di.component.DaggerActivityComponent;
import org.kidinov.kura_test.common.di.module.ActivityModule;
import org.kidinov.kura_test.file_picker.PdfFilePickerActivity;
import org.kidinov.kura_test.pdf_reader_opened_file.presentation.PdfReaderOpenedFileActivity;

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupComponent();

        ActivityComponent component = DaggerActivityComponent.builder()
                .applicationComponent(getApplicationComponent())
                .activityModule(new ActivityModule(this))
                .build();

        component.inject(this);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        getPresenter().create(savedInstanceState);
    }

    protected ApplicationComponent getApplicationComponent() {
        return ((App) getApplication()).getApplicationComponent();
    }

    public void startPdfFilePickerActivity(int code) {
        Intent intent = new Intent(this, PdfFilePickerActivity.class);
        intent.putExtra(FilePickerActivity.EXTRA_START_PATH, Environment.getExternalStorageDirectory().getPath());
        startActivityForResult(intent, code);
    }

    public void startPdfOpenedFileActivity(Uri data) {
        Intent intent = new Intent(this, PdfReaderOpenedFileActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setData(data);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPresenter().resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        getPresenter().pause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getPresenter().destroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        getPresenter().saveInstanceState(outState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getPresenter().onActivityResult(requestCode, resultCode, data);
    }


    protected abstract void setupComponent();

    protected abstract Presenter getPresenter();

}

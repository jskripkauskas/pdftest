package org.kidinov.kura_test.common;

import java.text.NumberFormat;
import java.text.ParsePosition;

public class NumberUtils {

    public static boolean isNumeric(String str) {
        NumberFormat formatter = NumberFormat.getInstance();
        ParsePosition pos = new ParsePosition(0);
        formatter.parse(str, pos);
        return str.length() == pos.getIndex();
    }
}
